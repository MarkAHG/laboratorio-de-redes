![# Welcome to StackEdit!](http://pagina.fciencias.unam.mx/sites/default/files/logoFC_2019.png)  
# Taller de Sistemas Operativos, Redes de Cómputo, Sistemas Distribuidos y Manejo de Información



## Practica 2: Servidor web con AWS EC2 

## Objetivo:

El alumno aprenderá a crear un servidor web (Apache 2) en la nube de AWS del tipo IaaS (EC2) , además preparará la infraestructura necesaria para un flujo de trabajo en la nube tipo C.D ( Entrega continua ) elemento fundamental de la metodología DevOps.

Finalmente el alumno comprenderá de manera práctica los conceptos de nube, IaaS (Infraestructura como servicio), IP Pública, puertos, politicas de seguridad

## Introducción

El protocolo HTTPS (HyperText Transfer Protocol Secure) es un protocolo de la Capa de aplicación que permite el envió de información cifrada usando los protocolos HTTP y SSL/TLS. El protocolo HTTP envía información en claro a través del medio, el protocolo SSL/TLS es el encargado de encapsular el protocolo HTTP para ser enviado de manera cifrada.

En está práctica utilizaremos estos conceptos colocando un servicio visible desde internet, usando la nube de amazon, en particular el servicio EC2, instalaremos un servidor web Apache 2 y colocaremos una página web de prueba (hola mundo)


## Prerequisitos
#### 1. Contar con usuario y contraseña de la cuenta AWS de Ciencias, en caso de no contar con ello no se podrá realizar la práctica, se revisaran los casos de alumnos especiales directamente conmigo 
#### 2. Crear un usuario con el perfil de Administrador, siguiendo el siguiente video: [Crear usuario Administrador](https://gitlab.com/ismael.andrade/laboratorio-de-redes/blob/master/lab2/videos/lab2-crear-usuario.mp4?expanded=true&viewer=rich)

## Desarrollo

1. Despues de crear el usuario con permisos de administrador es necesario crear una máquina AWS EC2 de la siguiente manera  [Crear Instancia EC2](https://gitlab.com/ismael.andrade/laboratorio-de-redes/blob/master/lab2/videos/lab2-ec2.mp4?expanded=true&viewer=rich)
2. Será necesario configurar el servidor apache (servidor web) de la siguiente manera: [Instalación de Servidor web Apache](https://gitlab.com/ismael.andrade/laboratorio-de-redes/blob/master/lab2/videos/lab2-apache2.mp4?expanded=true&viewer=rich)
3. Finalmente crear nuestro sitio web de prueba (hola mundo) en la siguiete liga: [Hola Mundo](https://gitlab.com/ismael.andrade/laboratorio-de-redes/blob/master/lab2/videos/lab2-hola-mundo.mp4?expanded=true&viewer=rich)

## Evaluación

1. ¿Qué es el concepto de nube y a qué se refiere el término Iaas? (describir con sus propias palabras aunque sea un renglon)
2. ¿Qé ventajas observas al utilizar la infraestructura que utilizamos en esta práctica? (describir con sus propias palabras aunque sea un renglon)
3. Colocar la url de amazon donde pueda visualizar el servicio creado en esta práctica
4. comentarios

### Notas adicionales
1. El reporte se entrega de manera individual.
3. Incluir las respuestas del Cuestionario en el reporte.
4. Se pueden agregar posibles errores, complicaciones, opiniones, críticas de la práctica o del laboratorio, o cualquier comentario relativo a la práctica.
5. Subir los archivos relacionados con la práctica a Moodle

### Errores comunes

